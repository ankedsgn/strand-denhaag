/*global module:false*/
  module.exports = function(grunt) {
    // Project configuration.
    grunt.initConfig({
      // Metadata.
      pkg: grunt.file.readJSON('package.json'),
      watch: {
        sass: {
          files: 'scss/{,*/}*.{scss,sass}',
          tasks: ['sass:dev']
        }
      },
      /**
       * Sass
       */
      sass: {
        dev: {
          options: {
            style: 'expanded'//, or 'compressed'
            //compass: true
          },
          files: {
            'styles/strand.css': 'scss/strand.scss'
          }
        },
      }
    });
    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    // Default task.
    grunt.registerTask('default', ['watch', 'sass:dev']);
  };
