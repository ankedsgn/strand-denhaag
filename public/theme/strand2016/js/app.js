/*$(document).foundation();*/

$(document).ready(function(){


	$('.strandtent-filter input').bind('change',function(e){
		update_strandtenten();
		console.log('kliks!');
	});

	/* MENU */
	$('.main-nav ul').addClass('hide-for-small-only');
	
	$('.menu-toggle').on('click', function() {
		$(this).next('ul').toggleClass('hide-for-small-only');
	});


});


function update_strandtenten()
{
	var flags = [];

	jQuery('.strandtent-filter input:checked').each(function(){
		flags[flags.length] = jQuery(this).val();
	});
	
	console.log (flags);
	if (flags.length == 0) {
		jQuery('.stranden-list li.strandtent').show();
	}
	else {
		jQuery('.stranden-list li.strandtent').each(function(){
			var show = false;

			var found = 0;
			for(var i=0; i < flags.length; i++) {
				if (jQuery(this).hasClass(flags[i])) {
					found++;
				}
			}

			if (found == flags.length) {
				show = true;
			}

			if (show) {
				jQuery(this).show();
			}
			else {
				jQuery(this).hide();
			}

		});
	}
}


