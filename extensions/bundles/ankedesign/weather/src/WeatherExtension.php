<?php

namespace Bolt\Extension\AnkeDesign\Weather;

use Bolt\Extension\SimpleExtension;

class WeatherExtension extends SimpleExtension
{
    const KEY_WEATHER_FORECAST = 'weatherForecast';
    const CACHE_EXPIRY         = 3600; // seconds

    public function twigGetWeatherForecast()
    {
        $app        = $this->getContainer();
        $config     = $this->getConfig();
        $httpClient = $app['guzzle.client'];
        $logger     = $app['logger.system'];
        $cache      = $app['cache'];

        if (! isset($config['key'])) {
            $logger->error("Missing API key!");
            return false;
        }

        if ($cache->contains(self::KEY_WEATHER_FORECAST)) {
            return $cache->fetch(self::KEY_WEATHER_FORECAST);
        }

        // $url = 'http://api.openweathermap.org/data/2.5/weather';
        $url = 'http://api.openweathermap.org/data/2.5/forecast';

        try {
            $response = $httpClient->request('GET', $url, [
                'query' => [
                'id'    => 2747599,
                'appid' => $config['key'],
                "lang"  => "nl",
                "units" => "metric",
                ]
            ]);

            $json    = $response->getBody()->getContents();
            $json    = mb_convert_encoding($json, 'UTF-8', 'UTF-8');
            $weather = json_decode($json, true);
            $cache->save(self::KEY_WEATHER_FORECAST, $weather, self::CACHE_EXPIRY);

        } catch (\Exception $e) {
            $logger->error("Failed fetching weather forecast: " . $e->getMessage(), ['event' => 'extension']);
            return false;
        }

        return $weather;
    }

    protected function registerTwigFunctions()
    {
        return [
            'getWeatherForecast' => [ 'twigGetWeatherForecast' ],
        ];
    }

}